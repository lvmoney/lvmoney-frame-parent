package com.lvmoney.frame.mqtt.provider.dto;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.smqtt.provider.dto
 * 版本信息: 版本1.0
 * 日期:2024/10/18
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/18 13:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PublishMessageDto<T> implements Serializable {
    private static final long serialVersionUID = 7038342555941820595L;
    /**
     * qos
     */
    private int qos;
    /**
     * retained
     */
    private Boolean retained;
    /**
     * topic
     */
    private String topic;
    /**
     * message
     */
    private T message;
}
