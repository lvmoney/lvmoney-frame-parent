package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/11/1
 * Copyright 四川XXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/11/1 10:33
 */
public interface MqttConnectService {
    /**
     * 连接
     *
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/11/1 10:35
     */
    void connect();

    /**
     * 不连接
     *
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/11/1 10:35
     */
    void notConnect();

    /**
     * 获得连接状态
     *
     * @throws
     * @return: java.lang.Boolean
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/11/1 10:35
     */
    Boolean getConnect();
}
