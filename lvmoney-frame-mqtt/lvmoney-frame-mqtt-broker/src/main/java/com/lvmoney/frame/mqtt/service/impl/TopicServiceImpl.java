package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/17
 * Copyright 四川XXXX科技有限公司
 */


import cn.hutool.core.bean.BeanUtil;
import com.google.common.collect.Sets;
import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.base.core.constant.MathOperationConstant;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.ro.TopicRo;
import com.lvmoney.frame.mqtt.service.MqttRedisService;
import com.lvmoney.frame.mqtt.service.TopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/17 11:32
 */
@Service
public class TopicServiceImpl implements TopicService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TopicServiceImpl.class);
    @Autowired
    MqttRedisService mqttRedisService;
    @Autowired
    MqttBrokerProp mqttBrokerProp;

    @Override
    public void subscribe(String clientId, String topicFilter) {
        NavigableMap<String, Set<String>> subscriptions = mqttRedisService.getSubscriptions();
        if (BeanUtil.isEmpty(subscriptions)) {
            subscriptions = new TreeMap<>();
        }
        subscriptions.compute(topicFilter, (key, clients) -> {
            if (clients == null) {
                clients = new HashSet<>();
            }
            clients.add(clientId);
            return clients;
        });
        TopicRo topicRo = new TopicRo();
        topicRo.setData(subscriptions);
        topicRo.setExpire(Long.valueOf(mqttBrokerProp.getSubscribeExpired()));
        mqttRedisService.saveSubscriptions(topicRo);
        LOGGER.info("Client " + clientId + " subscribed to: " + topicFilter);
    }

    @Override
    public void unsubscribe(String clientId, String topicFilter) {
        NavigableMap<String, Set<String>> subscriptions = mqttRedisService.getSubscriptions();
        subscriptions.computeIfPresent(topicFilter, (key, clients) -> {
            clients.remove(clientId);
            if (clients.isEmpty()) {
                return null;
            }
            return clients;
        });
        TopicRo topicRo = new TopicRo();
        topicRo.setData(subscriptions);
        topicRo.setExpire(Long.valueOf(mqttBrokerProp.getSubscribeExpired()));
        mqttRedisService.saveSubscriptions(topicRo);
    }

    @Override
    public Set<String> topicRouter(String topic) {
        NavigableMap<String, Set<String>> subscriptions = mqttRedisService.getSubscriptions();
        LOGGER.info("router to topic[{}] ", topic);
        // 从最具体的Topic Filter开始匹配，逐步放宽至通配符匹配
        NavigableSet<String> keys = subscriptions.navigableKeySet();
        Set<String> clientIds = Sets.newHashSet();
        for (String filter : keys) {
            if (isMatch(topic, filter)) {
                clientIds.addAll(subscriptions.get(filter));
            } else if (filter.contains(MathOperationConstant.OPERATION_SYMBOL_PLUS) || filter.contains(BaseConstant.HASH_MARK)) {
                break; // 已经尝试过最具体的，接下来的是更宽泛的通配符，不必继续
            }
        }
        return clientIds;
    }

    /**
     * 匹配算法，考虑了+和#的复杂情况
     *
     * @param topic:
     * @param filter:
     * @throws
     * @return: boolean
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:38
     */
    private boolean isMatch(String topic, String filter) {
        String[] topicParts = topic.split(BaseConstant.BACKSLASH);
        String[] filterParts = filter.split(BaseConstant.BACKSLASH);

        if (filterParts.length != topicParts.length) {
            return false;
        }

        for (int i = 0; i < filterParts.length; i++) {
            if (MathOperationConstant.OPERATION_SYMBOL_PLUS.equals(filterParts[i])) continue; // +匹配任意单个层级
            if (BaseConstant.HASH_MARK.equals(filterParts[i])) {
                if (i < topicParts.length - 1) return false; // #必须是最后一个或者单独存在
                break; // 匹配完成，#可以代表之后的所有层级
            }
            if (!filterParts[i].equals(topicParts[i])) {
                return false;
            }
        }
        return true;
    }

}
