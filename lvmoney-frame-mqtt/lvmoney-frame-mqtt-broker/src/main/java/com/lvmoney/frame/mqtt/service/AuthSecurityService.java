package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import com.lvmoney.frame.mqtt.dto.AuthInfoDto;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
public interface AuthSecurityService {
    /**
     * 新增权限
     *
     * @param authInfoDto:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:48
     */
    void addAuthSecurity(AuthInfoDto authInfoDto);

    /**
     * 删除权限
     *
     * @param name:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:48
     */
    void removeAuthSecurity(String name);

    /**
     * 权限校验
     *
     * @param authInfoDto:
     * @throws
     * @return: java.lang.Boolean
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:49
     */
    Boolean authSecurity(AuthInfoDto authInfoDto);

}
