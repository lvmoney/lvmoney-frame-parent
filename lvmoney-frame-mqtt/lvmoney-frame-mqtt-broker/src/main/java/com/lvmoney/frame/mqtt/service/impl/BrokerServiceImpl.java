package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import com.lvmoney.frame.mqtt.handler.ConnectionRateLimitHandler;
import com.lvmoney.frame.mqtt.handler.MqttMessageDebounceHandler;
import com.lvmoney.frame.mqtt.handler.MqttPreludeHandler;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.service.BrokerService;
import com.lvmoney.frame.mqtt.util.NettyUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/15 10:55
 */
@Service
public class BrokerServiceImpl implements BrokerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);
    @Autowired
    MqttPreludeHandler mqttPreludeHandler;
    @Autowired
    MqttMessageDebounceHandler mqttMessageDebounceHandler;
    @Autowired
    MqttBrokerProp mqttBrokerProp;
    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;

    ChannelFuture tcpChannelF;
    @Autowired
    ConnectionRateLimitHandler connRateLimitHandler;

    public void beforeBrokerStart() {

    }

    public void afterBrokerStop() {

    }

    @Override
    public void start() {
        try {
            LOGGER.info("Starting MQTT broker");
            beforeBrokerStart();
            LOGGER.debug("Starting server channel");

            tcpChannelF = this.bindTCPChannel();
            Channel channel = tcpChannelF.sync().channel();
            LOGGER.debug("Accepting mqtt connection over tcp channel at {}", channel.localAddress());


            LOGGER.info("MQTT broker started");
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void shutdown() {

        LOGGER.info("Shutting down MQTT broker");
        if (tcpChannelF != null) {
            tcpChannelF.channel().close().syncUninterruptibly();
            LOGGER.debug("Stopped accepting mqtt connection over tcp channel");
        }

        bossGroup.shutdownGracefully().syncUninterruptibly();
        LOGGER.debug("Boss group shutdown");
        workerGroup.shutdownGracefully().syncUninterruptibly();
        LOGGER.debug("Worker group shutdown");
        afterBrokerStop();
        LOGGER.info("MQTT broker shutdown");

    }

    private ChannelFuture bindTCPChannel() {
        ServerBootstrap b = new ServerBootstrap();
        bossGroup = NettyUtil.createEventLoopGroup(mqttBrokerProp.getMqttBossThreads(),
                newThreadFactory(MqttCommonConstant.NAME_BOSS_THREAD, false, Thread.NORM_PRIORITY));
        workerGroup = NettyUtil.createEventLoopGroup(mqttBrokerProp.getMqttWorkerThreads(),
                newThreadFactory(MqttCommonConstant.NAME_WORKER_THREAD, false, Thread.NORM_PRIORITY));
        b.group(bossGroup, workerGroup)
                .channel(NettyUtil.determineServerSocketChannelClass(bossGroup))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {

                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(MqttCommonConstant.NAME_CONN_LIMITER, connRateLimitHandler);
                        pipeline.addLast(MqttCommonConstant.NAME_TRAFFIC_SHAPER,
                                new ChannelTrafficShapingHandler(mqttBrokerProp.getWriteLimit(), mqttBrokerProp.getReadLimit()));
                        pipeline.addLast(MqttEncoder.class.getName(), MqttEncoder.INSTANCE);
                        pipeline.addLast(MqttDecoder.class.getName(), new MqttDecoder(mqttBrokerProp.getMaxBytesInMessage()));
                        pipeline.addLast(MqttCommonConstant.NAME_MESSAGE_DEBOUNCE_HANDLER, mqttMessageDebounceHandler);
                        pipeline.addLast(MqttCommonConstant.NAME_PRELUDE_HANDLER, mqttPreludeHandler);
                    }
                });

        // Bind and start to accept incoming connections.
        return b.bind(mqttBrokerProp.getPort());

    }

    private ThreadFactory newThreadFactory(String name, boolean daemon, int priority) {
        return new ThreadFactory() {
            private final AtomicInteger seq = new AtomicInteger();

            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                int s = seq.getAndIncrement();
                t.setName(s > 0 ? name + BaseConstant.DASH_LINE + s : name);
                t.setDaemon(daemon);
                t.setPriority(priority);
                return t;
            }
        };
    }
}
