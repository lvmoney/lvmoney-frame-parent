package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import cn.hutool.core.util.StrUtil;
import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.cache.lock.constant.LockConstant;
import com.lvmoney.frame.cache.lock.service.DistributedLockerService;
import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import com.lvmoney.frame.mqtt.ro.ClientInfoRo;
import com.lvmoney.frame.mqtt.util.ChannelUtil;
import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.service.MqttRedisService;
import com.lvmoney.frame.mqtt.service.MqttSessionService;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * 暂未找到 将 client信息存放到redis的方法
 * 主要是 ClientInfoDto 的 Channel 对象 放到redis后，取出来就变了
 * 思路是直接将java 对象ClientInfoDto存到redis
 *
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 16:59
 */
@Service
public class MqttSessionServiceImpl implements MqttSessionService {
    @Autowired
    MqttRedisService mqttRedisService;
    @Autowired
    MqttBrokerProp mqttBrokerProp;
    private final ConcurrentMap<String/* channelId */, ClientInfoDto> clientInfoTable =
            new ConcurrentHashMap();

    private final ConcurrentMap<String/* clientId */, String/*channelId*/> clientChannelIdTable =
            new ConcurrentHashMap<>();
    @Autowired
    DistributedLockerService distributedLockerService;
    /**
     * 服务名
     */
    @Value("${spring.application.name:lvmoney}")
    private String serverName;

    @Override
    public void addClientInfo(Channel channel) {
        String clientId = ChannelUtil.getClientId(channel);
        distributedLockerService.lock(MqttCommonConstant.MQTT_CLIENT_ID_LOCK_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName + BaseConstant.COLON + clientId, TimeUnit.SECONDS, LockConstant.LOCK_TIME);
        remove(clientId);
        clientChannelIdTable.putIfAbsent(clientId, channel.id().toString());
        InetSocketAddress ipSocket = (InetSocketAddress) channel.remoteAddress();


        ClientInfoDto clientInfoDto = new ClientInfoDto().setChannel(channel)
                .setClientId(clientId)
                .setIp(ipSocket.getAddress().getHostAddress())
                .setName(ChannelUtil.getName(channel))
                .setPassword(ChannelUtil.getPassword(channel))
                .setOnlineTime(LocalDateTime.now());
        clientInfoTable.putIfAbsent(channel.id().toString(), clientInfoDto);
        distributedLockerService.unlock(MqttCommonConstant.MQTT_CLIENT_ID_LOCK_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName + BaseConstant.COLON + clientId);

    }

    @Override
    public ClientInfoDto getClientInfo(String channelId) {
        return clientInfoTable.get(channelId);
    }

    @Override
    public void remove(Channel channel) {
        ClientInfoDto clientInfo = clientInfoTable.get(channel.id().toString());
        if (clientInfo != null) {
            clientInfoTable.remove(channel.id().toString());
            remove(clientInfo.getClientId());
        }
    }

    private void remove(String clientId) {
        try {
            String channelId = clientChannelIdTable.get(clientId);
            if (StrUtil.isNotBlank(channelId)) {
                removeClientInfo(channelId);
                clientChannelIdTable.remove(clientId);
            }
        } catch (Exception e) {

        }

    }

    private void removeClientInfo(String channelId) {
        ClientInfoDto clientInfo = clientInfoTable.get(channelId);
        if (clientInfo != null) {
            clientInfoTable.remove(channelId);
        }
    }

}
