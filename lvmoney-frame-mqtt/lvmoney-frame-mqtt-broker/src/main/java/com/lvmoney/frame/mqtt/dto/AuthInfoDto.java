package com.lvmoney.frame.mqtt.dto;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.dto
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 13:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthInfoDto implements Serializable {
    private static final long serialVersionUID = 405447054608450772L;
    /**
     * 账号
     */
    private String name;
    /**
     * 密码
     */
    private String password;
}
