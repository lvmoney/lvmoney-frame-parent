package com.lvmoney.frame.mqtt.handler;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.handler
 * 版本信息: 版本1.0
 * 日期:2024/10/17
 * Copyright 四川XXXX科技有限公司
 */


import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.mqtt.dto.AuthInfoDto;
import com.lvmoney.frame.mqtt.service.TopicService;
import com.lvmoney.frame.mqtt.util.ChannelUtil;
import com.lvmoney.frame.mqtt.service.AuthSecurityService;
import com.lvmoney.frame.mqtt.service.MqttSessionService;
import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.mqtt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static io.netty.handler.codec.mqtt.MqttMessageType.*;
import static io.netty.handler.codec.mqtt.MqttQoS.AT_LEAST_ONCE;
import static io.netty.handler.codec.mqtt.MqttQoS.AT_MOST_ONCE;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/17 9:40
 */
@Component
@ChannelHandler.Sharable
public class MqttConnectV3Handler extends ChannelDuplexHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MqttConnectV3Handler.class);
    private volatile boolean connected;
    @Autowired
    MqttSessionService mqttSessionService;
    @Autowired
    AuthSecurityService authSecurity;
    @Autowired
    TopicService topicService;


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        connected = false;
        mqttSessionService.remove(ctx.channel());
        super.channelInactive(ctx);
    }

    @Override
    public final void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        MqttMessage mqttMessage = (MqttMessage) msg;
        if (mqttMessage.fixedHeader() == null) {
            processDisconnectCompose(ctx);
            return;
        }

        switch (mqttMessage.fixedHeader().messageType()) {
            case CONNECT:
                processConnectCompose(ctx, (MqttConnectMessage) msg);
                break;
            case PUBLISH:
                if (checkConnected(ctx)) {
                    processPublish(ctx, (MqttPublishMessage) msg);
                }
                break;
            case SUBSCRIBE:
                if (checkConnected(ctx)) {
                    processSubscribe(ctx, (MqttSubscribeMessage) msg);
                }
                break;
            case UNSUBSCRIBE:
                if (checkConnected(ctx)) {
                    processUnsubscribe(ctx, (MqttUnsubscribeMessage) msg);
                }
                break;
            case PINGREQ:
                if (checkConnected(ctx)) {
                    ctx.writeAndFlush(new MqttMessage(new MqttFixedHeader(PINGRESP, false, AT_MOST_ONCE, false, 0)));
                }
                break;
            case DISCONNECT:
                if (checkConnected(ctx)) {
                    processDisconnectCompose(ctx);
                }
                break;
            default:
                break;

        }

    }

    private Boolean processAuthSecurity(ChannelHandlerContext ctx, MqttConnectMessage msg) throws Exception {
        String username = msg.payload().userName();
        String password = "";
        if (msg.variableHeader().hasPassword()) {
            try {
                password = new String(msg.payload().passwordInBytes(), BaseConstant.CHARACTER_ENCODE_UTF8_UPPER);
            } catch (Exception e) {
                LOGGER.error("password convert error msg[{}]", e.getMessage(), e);
            }
        }
        String clientId = msg.payload().clientIdentifier();
        ChannelUtil.setClientId(ctx.channel(), clientId);
        ChannelUtil.setName(ctx.channel(), username);
        ChannelUtil.setPassword(ctx.channel(), password);
        AuthInfoDto authInfoDto = new AuthInfoDto();
        authInfoDto.setName(username);
        authInfoDto.setPassword(password);
        return authSecurity.authSecurity(authInfoDto);
    }

    private MqttConnAckMessage createMqttConnAckMsg(MqttConnectReturnCode returnCode) {
        MqttFixedHeader mqttFixedHeader =
                new MqttFixedHeader(CONNACK, false, AT_MOST_ONCE, false, 0);
        MqttConnAckVariableHeader mqttConnAckVariableHeader =
                new MqttConnAckVariableHeader(returnCode, true);
        return new MqttConnAckMessage(mqttFixedHeader, mqttConnAckVariableHeader);
    }


    private void processConnectCompose(ChannelHandlerContext ctx, MqttConnectMessage msg) throws Exception {
        if (processAuthSecurity(ctx, msg)) {
            connected = true;
            processConnect(ctx, msg);
            ctx.writeAndFlush(createMqttConnAckMsg(MqttConnectReturnCode.CONNECTION_ACCEPTED));
        } else {
            connected = false;
            ctx.writeAndFlush(createMqttConnAckMsg(MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD));
        }
    }

    public void processDisconnectCompose(ChannelHandlerContext ctx) {
        LOGGER.info("disconnect channelId[{}]", ctx.channel().id().toString());
        connected = false;
        try {
            processDisconnect(ctx);
        } catch (Exception e) {
            LOGGER.error("disconnect error msg[{}]", e.getMessage(), e);
        } finally {
            ctx.channel().close();
            ctx.close();
        }
    }

    public void processDisconnect(ChannelHandlerContext ctx) {
        mqttSessionService.remove(ctx.channel());
    }

    public void processConnect(ChannelHandlerContext ctx, MqttConnectMessage msg) {
        LOGGER.info("connect channelId[{}]", ctx.channel().id().toString());
        mqttSessionService.addClientInfo(ctx.channel());
    }

    public void processPublish(ChannelHandlerContext ctx, MqttPublishMessage msg) {
        LOGGER.info("publish channelId[{}]", ctx.channel().id().toString());
        String topicName = msg.variableHeader().topicName();
        topicService.topicRouter(topicName).forEach(channelId -> {
            ClientInfoDto clientInfoDto = mqttSessionService.getClientInfo(channelId);
            if (ObjectUtil.isNotNull(clientInfoDto)) {
                LOGGER.debug("PUBLISH - clientId: {}, topic: {}, Qos: {}", clientInfoDto.getClientId(), topicName, msg.fixedHeader().qosLevel().value());
                Channel channel = clientInfoDto.getChannel();
                if (channel != null) {
                    channel.writeAndFlush(msg);
                }
            }
        });
    }

    public void processSubscribe(ChannelHandlerContext ctx, MqttSubscribeMessage msg) {
        LOGGER.info("subscribe channelId[{}]", ctx.channel().id().toString());
        List<Integer> grantedQoSList = Lists.newArrayList();
        for (MqttTopicSubscription subscription : msg.payload().topicSubscriptions()) {
            String topic = subscription.topicName();
            MqttQoS qoS = subscription.qualityOfService();
            topicService.subscribe(ctx.channel().id().toString(), topic);
            grantedQoSList.add(qoS.value());
        }
        ctx.writeAndFlush(createSubAckMessage(msg.variableHeader().messageId(), grantedQoSList));
    }

    public void processUnsubscribe(ChannelHandlerContext ctx, MqttUnsubscribeMessage msg) {
        LOGGER.info("unsubscribe channelId[{}]", ctx.channel().id().toString());
        for (String topic : msg.payload().topics()) {
            topicService.unsubscribe(ctx.channel().id().toString(), topic);
        }
        ctx.writeAndFlush(createUnSubAckMessage(msg.variableHeader().messageId()));
    }

    public Boolean checkConnected(ChannelHandlerContext ctx) {
        if (connected) {
            return true;
        } else {
            ctx.channel().close();
            ctx.close();
            return false;
        }
    }

    private static MqttSubAckMessage createSubAckMessage(Integer msgId, List<Integer> grantedQoSList) {
        MqttFixedHeader mqttFixedHeader =
                new MqttFixedHeader(SUBACK, false, AT_LEAST_ONCE, false, 0);
        MqttMessageIdVariableHeader mqttMessageIdVariableHeader = MqttMessageIdVariableHeader.from(msgId);
        MqttSubAckPayload mqttSubAckPayload = new MqttSubAckPayload(grantedQoSList);
        return new MqttSubAckMessage(mqttFixedHeader, mqttMessageIdVariableHeader, mqttSubAckPayload);
    }

    private MqttMessage createUnSubAckMessage(int msgId) {
        MqttFixedHeader mqttFixedHeader =
                new MqttFixedHeader(UNSUBACK, false, AT_LEAST_ONCE, false, 0);
        MqttMessageIdVariableHeader mqttMessageIdVariableHeader = MqttMessageIdVariableHeader.from(msgId);
        return new MqttMessage(mqttFixedHeader, mqttMessageIdVariableHeader);
    }
}
