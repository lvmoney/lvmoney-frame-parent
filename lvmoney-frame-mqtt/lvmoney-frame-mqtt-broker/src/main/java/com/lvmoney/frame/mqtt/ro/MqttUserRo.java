package com.lvmoney.frame.mqtt.ro;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.ro
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.ro.item.UserInfoItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 14:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MqttUserRo implements Serializable {
    private static final long serialVersionUID = -4581200715103513249L;
    /**
     * 失效时间
     */

    private Long expire;
    /**
     * 把mqtt的user信息统一放到redis中
     */
    private Map<String, UserInfoItem> data;
}
