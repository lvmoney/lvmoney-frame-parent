package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/11/1
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.ro.ConnectRo;
import com.lvmoney.frame.mqtt.service.MqttConnectService;
import com.lvmoney.frame.mqtt.service.MqttRedisService;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/11/1 10:36
 */
@Service
public class MqttConnectServiceImpl implements MqttConnectService {
    @Autowired
    MqttRedisService mqttRedisService;
    @Autowired
    MqttBrokerProp mqttBrokerProp;

    @Override
    public void connect() {
        ConnectRo connectRo = new ConnectRo().setConnected(true).setExpire(Long.valueOf(mqttBrokerProp.getConnectedExpired()));
        mqttRedisService.saveConnected(connectRo);
    }

    @Override
    public void notConnect() {
        ConnectRo connectRo = new ConnectRo().setConnected(false).setExpire(Long.valueOf(mqttBrokerProp.getConnectedExpired()));
        mqttRedisService.saveConnected(connectRo);
    }

    @Override
    public Boolean getConnect() {
        Boolean connected = mqttRedisService.getConnected();
        return connected;
    }
}
