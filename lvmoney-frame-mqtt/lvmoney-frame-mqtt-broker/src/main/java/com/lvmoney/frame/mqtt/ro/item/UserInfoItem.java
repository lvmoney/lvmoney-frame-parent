package com.lvmoney.frame.mqtt.ro.item;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.ro.item
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0
 * 2024/10/16 14:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoItem implements Serializable {
    private static final long serialVersionUID = -8359262921540852144L;
    /**
     * 账号
     */
    private String name;
    /**
     * 密码
     */
    private String password;
}
