package com.lvmoney.frame.mqtt.handler;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import com.google.common.util.concurrent.RateLimiter;
import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@Component
@ChannelHandler.Sharable
public class ConnectionRateLimitHandler extends ChannelDuplexHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionRateLimitHandler.class);

    @Autowired
    MqttBrokerProp mqttBrokerProp;


    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        RateLimiter rateLimiter = RateLimiter.create(mqttBrokerProp.getConnectRateLimit());
        if (rateLimiter.tryAcquire()) {
            ctx.fireChannelActive();
        } else {
            LOGGER.warn("Connection dropped due to exceed limit");
            // close the connection randomly
            ctx.channel().config().setAutoRead(false);
            ctx.executor().schedule(() -> {
                if (ctx.channel().isActive()) {
                    ctx.close();
                }
            }, ThreadLocalRandom.current().nextLong(MqttCommonConstant.RANDOM_ORIGIN, MqttCommonConstant.RANDOM_ROUND), TimeUnit.MILLISECONDS);
        }
    }

}
