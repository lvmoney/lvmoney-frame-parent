package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import com.lvmoney.frame.core.util.Md5Util;
import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import com.lvmoney.frame.mqtt.dto.AuthInfoDto;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.ro.MqttUserRo;
import com.lvmoney.frame.mqtt.ro.item.UserInfoItem;
import com.lvmoney.frame.mqtt.service.AuthSecurityService;
import com.lvmoney.frame.mqtt.service.MqttRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@Service
public class AuthSecurityServiceImpl implements AuthSecurityService {
    @Autowired
    MqttRedisService mqttRedisService;
    @Autowired
    MqttBrokerProp mqttBrokerProp;

    @Override
    public void addAuthSecurity(AuthInfoDto authInfo) {
        String password = Md5Util.digest(MqttCommonConstant.PASS_CONFUSION_CODE + authInfo.getPassword());
        authInfo.setPassword(password);
        //test
        MqttUserRo mqttUserRo = new MqttUserRo();
        Map<String, UserInfoItem> data = new HashMap();
        data.put(authInfo.getName(), new UserInfoItem(authInfo.getName(), authInfo.getPassword()));
        mqttUserRo.setData(data);
        mqttUserRo.setExpire(Long.valueOf(mqttBrokerProp.getUserExpired()));
        mqttRedisService.saveUser(mqttUserRo);
    }

    @Override
    public void removeAuthSecurity(String name) {
        mqttRedisService.removeUserByName(new ArrayList() {{
            add(name);
        }});
    }

    @Override
    public Boolean authSecurity(AuthInfoDto authInfo) {
        AuthInfoDto dbAuthInfo = mqttRedisService.getUserByName(authInfo.getName());
        if (dbAuthInfo != null) {
            String password = Md5Util.digest(MqttCommonConstant.PASS_CONFUSION_CODE + authInfo.getPassword());
            return dbAuthInfo.getPassword().equals(password);
        } else {
            return false;
        }
    }


}
