package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import com.lvmoney.frame.mqtt.ro.ClientInfoRo;
import com.lvmoney.frame.mqtt.ro.ConnectRo;
import com.lvmoney.frame.mqtt.ro.MqttUserRo;
import com.lvmoney.frame.mqtt.ro.TopicRo;
import com.lvmoney.frame.mqtt.dto.AuthInfoDto;

import java.util.List;
import java.util.NavigableMap;
import java.util.Set;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 14:05
 */
public interface MqttRedisService {
    /**
     * 将用户数据存入redis map
     *
     * @param mqttUserRo:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 14:34
     */
    void saveUser(MqttUserRo mqttUserRo);

    /**
     * 通过名称获得用户
     *
     * @param name:
     * @throws
     * @return: com.lvmoney.frame.mqtt.dto.AuthInfoDto
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 14:37
     */
    AuthInfoDto getUserByName(String name);

    /**
     * 批量删除name
     *
     * @param names:
     * @throws
     * @return: boolean
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 16:39
     */
    boolean removeUserByName(List<String> names);

    /**
     * 获得订阅者数据
     *
     * @throws
     * @return: java.util.NavigableMap<java.lang.String, java.util.Set < java.lang.String>>
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/18 10:35
     */

    NavigableMap<String, Set<String>> getSubscriptions();

    /**
     * 订阅者信息放到redis
     *
     * @param topicRo:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/28 9:13
     */
    void saveSubscriptions(TopicRo topicRo);

    /**
     * 保存client 数据
     *
     * @param clientInfoRo:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/28 9:20
     */
    void saveClient(ClientInfoRo clientInfoRo);

    /**
     * 通过clientId获得 client
     *
     * @param clientId:
     * @throws
     * @return: com.lvmoney.frame.mqtt.dto.ClientInfoDto
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/28 9:21
     */
    ClientInfoDto getByClientId(String clientId);

    void saveConnected(ConnectRo connectRo);


    Boolean getConnected();
}
