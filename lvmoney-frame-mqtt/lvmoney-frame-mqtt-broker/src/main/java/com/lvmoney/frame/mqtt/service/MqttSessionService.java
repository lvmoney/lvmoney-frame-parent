package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import io.netty.channel.Channel;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 16:57
 */
public interface MqttSessionService {
    /**
     * 新增客户端信息
     *
     * @param channel:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 16:58
     */
    void addClientInfo(Channel channel);

    /**
     * @param channelId:
     * @throws
     * @return: com.lvmoney.frame.mqtt.dto.ClientInfoDto
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/17 9:13
     */
    ClientInfoDto getClientInfo(String channelId);

    /**
     * 删除channel
     *
     * @param channel:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 16:58
     */
    void remove(Channel channel);
}
