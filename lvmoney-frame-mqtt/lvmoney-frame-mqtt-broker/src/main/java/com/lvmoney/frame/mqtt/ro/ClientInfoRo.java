package com.lvmoney.frame.mqtt.ro;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.ro
 * 版本信息: 版本1.0
 * 日期:2024/10/28
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.concurrent.ConcurrentMap;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/28 9:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ClientInfoRo implements Serializable {
    private static final long serialVersionUID = -7548880092366901218L;
    /**
     * 客户端id
     */
    private String ClientId;
    /**
     * 核心数据
     */
    private ClientInfoDto data;

    /**
     * 失效时间
     */
    private Long expired;
}
