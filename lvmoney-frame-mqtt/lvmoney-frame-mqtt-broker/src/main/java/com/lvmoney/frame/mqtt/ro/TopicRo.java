package com.lvmoney.frame.mqtt.ro;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.ro
 * 版本信息: 版本1.0
 * 日期:2024/10/17
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.NavigableMap;
import java.util.Set;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/17 13:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicRo implements Serializable {
    private static final long serialVersionUID = 1313759719160916304L;
    /**
     * 失效时间
     */

    private Long expire;

    private NavigableMap<String, Set<String>> data;
}
