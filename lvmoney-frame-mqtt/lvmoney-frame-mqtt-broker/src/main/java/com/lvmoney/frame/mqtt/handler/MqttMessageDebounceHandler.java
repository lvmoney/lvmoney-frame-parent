package com.lvmoney.frame.mqtt.handler;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.mqtt.MqttMessage;
import io.netty.util.ReferenceCountUtil;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@Component
@ChannelHandler.Sharable
public class MqttMessageDebounceHandler extends ChannelDuplexHandler {

    private final Queue<MqttMessage> buffer = new LinkedList<>();
    private boolean readOne = false;

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        MqttMessage msg;
        while ((msg = buffer.poll()) != null) {
            ReferenceCountUtil.release(msg);
        }
        super.channelInactive(ctx);
    }

    @Override
    public void read(ChannelHandlerContext ctx) {
        if (ctx.channel().config().isAutoRead()) {
            MqttMessage msg;
            while ((msg = buffer.poll()) != null) {
                ctx.fireChannelRead(msg);
            }
            ctx.read();
        } else {
            MqttMessage msg = buffer.poll();
            if (msg != null) {
                ctx.fireChannelRead(msg);
            } else {
                readOne = true;
                ctx.read();
            }
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        assert msg instanceof MqttMessage;
        if (ctx.channel().config().isAutoRead()) {
            ctx.fireChannelRead(msg);
        } else {
            buffer.offer((MqttMessage) msg);
            if (readOne) {
                MqttMessage mqttMsg = buffer.poll();
                ctx.fireChannelRead(mqttMsg);
                readOne = false;
            }
        }
    }
}
