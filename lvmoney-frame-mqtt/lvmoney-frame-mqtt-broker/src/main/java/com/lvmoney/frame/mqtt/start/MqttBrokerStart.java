package com.lvmoney.frame.mqtt.start;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.start
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.dto.AuthInfoDto;
import com.lvmoney.frame.mqtt.prop.MqttBrokerProp;
import com.lvmoney.frame.mqtt.service.AuthSecurityService;
import com.lvmoney.frame.mqtt.service.BrokerService;
import com.lvmoney.frame.pool.thread.config.ExecutorTaskConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/15 14:19
 */
@Service
public class MqttBrokerStart implements CommandLineRunner {
    @Autowired
    ExecutorTaskConfig executorTaskConfig;
    private static final Logger LOGGER = LoggerFactory.getLogger(MqttBrokerStart.class);
    @Autowired
    BrokerService brokerService;
    @Autowired
    MqttBrokerProp mqttBrokerProp;
    @Autowired
    AuthSecurityService addAuthSecurity;

    public void run() {
        try {
            brokerService.start();
            this.initAuth();
        } catch (Exception e) {
            LOGGER.error("mqtt broker error msg[{}]", e.getMessage(), e);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        ThreadPoolTaskExecutor asyncExecutor = executorTaskConfig.getAsyncExecutor();
        asyncExecutor.submit(new Thread(() -> {
            this.run();
        }));
    }

    private void initAuth() throws Exception {
        AuthInfoDto authInfoDto1 = new AuthInfoDto();
        authInfoDto1.setName("device:85211001");
        authInfoDto1.setPassword("a123456");
        addAuthSecurity.addAuthSecurity(authInfoDto1);
        AuthInfoDto authInfoDto2 = new AuthInfoDto();
        authInfoDto2.setName("device:85211002");
        authInfoDto2.setPassword("a123456");
        addAuthSecurity.addAuthSecurity(authInfoDto2);
    }
}
