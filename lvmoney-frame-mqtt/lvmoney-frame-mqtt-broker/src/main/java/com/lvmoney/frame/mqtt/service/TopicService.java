package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/17
 * Copyright 四川XXXX科技有限公司
 */


import com.google.common.collect.Sets;
import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.base.core.constant.MathOperationConstant;

import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/17 11:31
 */
public interface TopicService {
    /**
     * 订阅Topic Filter
     *
     * @param clientId:
     * @param topicFilter:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:37
     */
    void subscribe(String clientId, String topicFilter);

    /**
     * 取消订阅
     *
     * @param clientId:
     * @param topicFilter:
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:38
     */
    void unsubscribe(String clientId, String topicFilter);

    /**
     * 发布消息到指定Topic
     *
     * @param topic:
     * @throws
     * @return: java.util.Set<java.lang.String>
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/10/16 13:38
     */
    Set<String> topicRouter(String topic);


}
