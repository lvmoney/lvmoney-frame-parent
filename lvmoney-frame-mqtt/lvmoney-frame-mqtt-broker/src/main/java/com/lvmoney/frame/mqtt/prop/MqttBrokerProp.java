package com.lvmoney.frame.mqtt.prop;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.prop
 * 版本信息: 版本1.0
 * 日期:2024/10/11
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
public class MqttBrokerProp {
    /**
     * 连接数限制
     */
    @Value("${frame.mqtt.connectRateLimit:1000}")
    int connectRateLimit;
    /**
     * 写入限制
     * 512 * 1024
     */
    @Value("${frame.mqtt.writeLimit:524288}")
    long writeLimit;
    /**
     * 读限制
     */
    @Value("${frame.mqtt.readLimit:524288}")
    long readLimit;
    /**
     * 最大消息长度
     */
    @Value("${frame.mqtt.maxBytesInMessage:262144}")
    int maxBytesInMessage;
    /**
     * 主线程数
     */
    @Value("${frame.mqtt.mqttBossThreads:64}")
    int mqttBossThreads;
    /**
     * 运行线程数
     */
    @Value("${frame.mqtt.mqttWorkerThreads:64}")
    int mqttWorkerThreads;
    /**
     * 端口
     */
    @Value("${frame.mqtt.port:9000}")
    int port;
    /**
     * 用户失效时间，默认永久有效
     */
    @Value("${frame.mqtt.userExpired:-1}")
    String userExpired;

    /**
     * 客户端失效时间，默认永久有效
     */
    @Value("${frame.mqtt.clientExpired:-1}")
    String clientExpired;


    /**
     * 订阅者失效时间，默认永久有效
     */
    @Value("${frame.mqtt.subscribeExpired:-1}")
    String subscribeExpired;
    /**
     * 连接状态，默认永久有效
     */
    @Value("${frame.mqtt.connectedExpired:-1}")
    String connectedExpired;
}
