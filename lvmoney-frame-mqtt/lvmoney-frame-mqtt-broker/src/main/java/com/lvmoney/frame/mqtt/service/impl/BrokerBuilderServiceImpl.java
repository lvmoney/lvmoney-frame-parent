package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.core.util.SnowflakeIdFactoryUtil;
import com.lvmoney.frame.mqtt.service.BrokerBuilderService;
import org.springframework.stereotype.Service;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/15 10:54
 */
@Service
public class BrokerBuilderServiceImpl implements BrokerBuilderService {
    @Override
    public String brokerId() {
        String brokerId = String.valueOf(SnowflakeIdFactoryUtil.nextId());

        return brokerId;
    }

}
