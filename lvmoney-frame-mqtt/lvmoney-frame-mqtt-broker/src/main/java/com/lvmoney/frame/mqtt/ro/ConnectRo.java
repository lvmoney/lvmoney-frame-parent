package com.lvmoney.frame.mqtt.ro;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.ro
 * 版本信息: 版本1.0
 * 日期:2024/11/1
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/11/1 10:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ConnectRo implements Serializable {

    private static final long serialVersionUID = 4830284586742484926L;
    /**
     * 失效时间
     */

    private Long expire;
    /**
     * 是否连接
     */
    private boolean connected;
}
