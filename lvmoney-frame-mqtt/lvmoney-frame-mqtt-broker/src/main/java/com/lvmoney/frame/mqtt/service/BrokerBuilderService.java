package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */



/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/15 10:49
 */
public interface BrokerBuilderService {
    /**
     * borker  id
     *
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/9/30 11:15
     */
    String brokerId();

}
