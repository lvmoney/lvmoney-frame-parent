package com.lvmoney.frame.mqtt.dto;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.dto
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import io.netty.channel.Channel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 17:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ClientInfoDto implements Serializable {
    private static final long serialVersionUID = -3513277707442715400L;

    /**
     * channel
     */
    private Channel channel;
    /**
     * channelId
     */
    private String channelId;
    /**
     * ip
     */
    private String ip;
    /**
     * onlineTime
     */
    private LocalDateTime onlineTime;
    /**
     * clientId
     */
    private String clientId;
    /**
     * 密码
     */
    private String password;
    /**
     * name
     */
    private String name;
}
