package com.lvmoney.frame.mqtt.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/16
 * Copyright 四川XXXX科技有限公司
 */


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.lvmoney.frame.base.core.constant.BaseConstant;
import com.lvmoney.frame.cache.redis.service.BaseRedisService;
import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import com.lvmoney.frame.mqtt.dto.ClientInfoDto;
import com.lvmoney.frame.mqtt.ro.ClientInfoRo;
import com.lvmoney.frame.mqtt.ro.ConnectRo;
import com.lvmoney.frame.mqtt.ro.MqttUserRo;
import com.lvmoney.frame.mqtt.ro.TopicRo;
import com.lvmoney.frame.mqtt.service.MqttRedisService;
import com.lvmoney.frame.mqtt.dto.AuthInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/16 14:22
 */
@Service
public class MqttRedisServiceImpl implements MqttRedisService {
    /**
     * 服务名
     */
    @Value("${spring.application.name:lvmoney}")
    private String serverName;
    @Autowired
    BaseRedisService baseRedisService;
    private static final Logger LOGGER = LoggerFactory.getLogger(MqttRedisServiceImpl.class);

    @Override
    public void saveUser(MqttUserRo mqttUserRo) {
        baseRedisService.addMap(MqttCommonConstant.MQTT_USER_GROUP_PREFIX + BaseConstant.COLON + serverName, mqttUserRo.getData(), mqttUserRo.getExpire());
    }

    @Override
    public AuthInfoDto getUserByName(String name) {
        try {
            Object obj = baseRedisService.getByMapKey(MqttCommonConstant.MQTT_USER_GROUP_PREFIX + BaseConstant.COLON + serverName, name);
            AuthInfoDto authInfoDto = JSON.parseObject(obj.toString(), new TypeReference<AuthInfoDto>() {
            });
            return authInfoDto;
        } catch (Exception e) {
            LOGGER.error("通过name获得mqtt账号详情信息报错:{}", e);
            return null;
        }
    }

    @Override
    public boolean removeUserByName(List<String> names) {
        Long result = baseRedisService.deleteByMapKey(MqttCommonConstant.MQTT_USER_GROUP_PREFIX + BaseConstant.COLON + serverName, names.toArray(new String[0]));
        return result > 0 ? true : false;
    }


    @Override
    public NavigableMap<String, Set<String>> getSubscriptions() {
        try {
            Object obj = baseRedisService.getByKey(MqttCommonConstant.MQTT_SUBSCRIPTION_PREFIX + BaseConstant.COLON + serverName);
            if (BeanUtil.isNotEmpty(obj)) {
                Map<String, List<String>> map = JSON.parseObject(obj.toString(), new TypeReference<HashMap>() {
                });
                NavigableMap<String, Set<String>> result = new TreeMap<>();
                map.forEach((k, v) -> {
                    Set<String> set = v.stream().collect(Collectors.toSet());
                    result.put(k, set);
                });
                return result;
            }
            return null;
        } catch (Exception e) {
            LOGGER.error("通过clientId获得mqtt客户端详情信息报错:{}", e);
            return null;
        }
    }

    @Override
    public void saveSubscriptions(TopicRo topicRo) {
        baseRedisService.setString(MqttCommonConstant.MQTT_SUBSCRIPTION_PREFIX + BaseConstant.COLON + serverName, topicRo.getData(), topicRo.getExpire());
    }

    @Override
    public void saveClient(ClientInfoRo clientInfoRo) {
        baseRedisService.saveObj(MqttCommonConstant.MQTT_CLIENT_GROUP_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName + BaseConstant.COLON + clientInfoRo.getClientId(), clientInfoRo.getData(), clientInfoRo.getExpired());
    }

    @Override
    public ClientInfoDto getByClientId(String clientId) {
        Object obj = baseRedisService.getObj(MqttCommonConstant.MQTT_CLIENT_GROUP_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName + BaseConstant.COLON + clientId);
        return (ClientInfoDto) obj;
    }

    @Override
    public void saveConnected(ConnectRo connectRo) {
        baseRedisService.setString(MqttCommonConstant.MQTT_CONNECT_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName, connectRo, connectRo.getExpire());
    }

    @Override
    public Boolean getConnected() {
        Object obj = baseRedisService.getByKey(MqttCommonConstant.MQTT_CONNECT_PREFIX + BaseConstant.CONNECTOR_UNDERLINE + serverName);
        ConnectRo connectRo = JSON.parseObject(obj.toString(), new TypeReference<ConnectRo>() {
        });
        return connectRo.isConnected();
    }

}
