package com.lvmoney.frame.mqtt.dto;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.dto
 * 版本信息: 版本1.0
 * 日期:2024/10/30
 * Copyright 四川XXXX科技有限公司
 */


import io.netty.channel.Channel;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/30 15:18
 */
public class ChannelDto implements Serializable {
    private static final long serialVersionUID = -2633067428796499438L;
    private final Channel channel;

    public ChannelDto(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

}
