package com.lvmoney.frame.mqtt.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0
 * 2024/10/15 10:48
 */
public interface BrokerService {
    /**
     * 启动
     *
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/9/30 11:14
     */
    void start();

    /**
     * 关闭
     *
     * @throws
     * @return: void
     * @author: lvmoney /四川XXXX科技有限公司
     * @date: 2024/9/30 11:14
     */
    void shutdown();

}
