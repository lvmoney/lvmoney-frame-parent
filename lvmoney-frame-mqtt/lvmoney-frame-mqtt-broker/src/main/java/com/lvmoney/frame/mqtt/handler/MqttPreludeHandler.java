package com.lvmoney.frame.mqtt.handler;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.TooLongFrameException;
import io.netty.handler.codec.mqtt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.*;
import static io.netty.handler.codec.mqtt.MqttMessageType.CONNECT;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@Component
@ChannelHandler.Sharable
public class MqttPreludeHandler extends ChannelDuplexHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MqttPreludeHandler.class);
    @Autowired
    MqttConnectV3Handler mqttConnectV3Handler;

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        ctx.fireChannelInactive();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        assert msg instanceof MqttMessage;
        // stop reading next message and resume reading once finish processing current one
        ctx.channel().config().setAutoRead(false);
        MqttMessage message = (MqttMessage) msg;
        if (!message.decoderResult().isSuccess()) {
            Throwable cause = message.decoderResult().cause();
            if (cause instanceof MqttUnacceptableProtocolVersionException) {
                closeChannelWithRandomDelay(ctx);
                return;
            }
            if (message.fixedHeader() != null && message.fixedHeader().messageType() != CONNECT) {
                closeChannelWithRandomDelay(ctx);
                return;
            }
            if (message.variableHeader() instanceof MqttConnectVariableHeader) {
                MqttConnectVariableHeader connVarHeader = (MqttConnectVariableHeader) message.variableHeader();
                switch (connVarHeader.version()) {
                    case 3:
                    case 4:
                        if (cause instanceof TooLongFrameException) {
                            closeChannelWithRandomDelay(ctx);
                        } else if (cause instanceof MqttIdentifierRejectedException) {
                            closeChannelWithRandomDelay(ctx,
                                    MqttMessageBuilders.connAck()
                                            .returnCode(CONNECTION_REFUSED_IDENTIFIER_REJECTED)
                                            .build());
                        } else {
                            closeChannelWithRandomDelay(ctx);
                        }
                        return;
                    case 5:
                    default:
                        if (cause instanceof TooLongFrameException) {
                            MqttProperties.StringProperty stringProperty = new MqttProperties.StringProperty(MqttProperties.MqttPropertyType.REASON_STRING.value(),
                                    cause.getMessage());
                            MqttProperties mqttProperties = new MqttProperties();
                            mqttProperties.add(stringProperty);
                            closeChannelWithRandomDelay(ctx,
                                    MqttMessageBuilders.connAck()
                                            .properties(mqttProperties)
                                            .returnCode(CONNECTION_REFUSED_PACKET_TOO_LARGE)
                                            .build());
                        } else if (cause instanceof MqttIdentifierRejectedException) {
                            // decode mqtt connect packet error
                            MqttProperties.StringProperty stringProperty = new MqttProperties.StringProperty(MqttProperties.MqttPropertyType.REASON_STRING.value(),
                                    cause.getMessage());
                            MqttProperties mqttProperties = new MqttProperties();
                            mqttProperties.add(stringProperty);
                            closeChannelWithRandomDelay(ctx,
                                    MqttMessageBuilders.connAck()
                                            .properties(mqttProperties)
                                            .returnCode(CONNECTION_REFUSED_CLIENT_IDENTIFIER_NOT_VALID)
                                            .build());
                        } else {
                            // according to [MQTT-4.13.1-1]
                            MqttProperties.StringProperty stringProperty = new MqttProperties.StringProperty(MqttProperties.MqttPropertyType.REASON_STRING.value(),
                                    cause.getMessage());
                            MqttProperties mqttProperties = new MqttProperties();
                            mqttProperties.add(stringProperty);
                            closeChannelWithRandomDelay(ctx,
                                    MqttMessageBuilders.connAck()
                                            .properties(mqttProperties)
                                            .returnCode(CONNECTION_REFUSED_MALFORMED_PACKET)
                                            .build());
                        }
                        return;
                }
            } else {
                closeChannelWithRandomDelay(ctx);
                return;
            }
        } else if (!(message instanceof MqttConnectMessage)) {
            // according to [MQTT-3.1.0-1]
            closeChannelWithRandomDelay(ctx);
            // LOGGER.warn("First packet must be mqtt connect message: remote={}", remoteAddr);
            return;
        }

        MqttConnectMessage connectMessage = (MqttConnectMessage) message;
        switch (connectMessage.variableHeader().version()) {
            case 3:
            case 4:
                ctx.pipeline().addAfter(ctx.executor(),
                        MqttCommonConstant.NAME_PRELUDE_HANDLER, MqttCommonConstant.NAME_CONNECT_HANDLER, mqttConnectV3Handler);
                ctx.channel().config().setAutoRead(true);
                // delegate to MQTT 3 handler
                ctx.fireChannelRead(connectMessage);
                ctx.pipeline().remove(this);
                break;
            case 5:
                // ctx.pipeline().addAfter(ctx.executor(),
                //         MqttPreludeHandler.NAME, MQTTV5ConnectHandler.NAME, new MQTTV5ConnectHandler());
                // // delegate to MQTT 5 handler
                // ctx.fireChannelRead(connectMessage);
                // ctx.pipeline().remove(this);
                break;
            default:
                LOGGER.warn("Unsupported protocol version: {}", connectMessage.variableHeader().version());
        }
    }

    private void closeChannelWithRandomDelay(ChannelHandlerContext ctx) {
        closeChannelWithRandomDelay(ctx, null);
    }

    private void closeChannelWithRandomDelay(ChannelHandlerContext ctx, MqttMessage msg) {
        ctx.executor().schedule(() -> {
            if (!ctx.channel().isActive()) {
                return;
            }
            if (msg != null) {
                ctx.writeAndFlush(msg).addListener(ChannelFutureListener.CLOSE);
            } else {
                ctx.channel().close();
            }
        }, ThreadLocalRandom.current().nextLong(MqttCommonConstant.RANDOM_ORIGIN, MqttCommonConstant.RANDOM_ROUND), TimeUnit.MILLISECONDS);
    }

}
