package com.lvmoney.frame.mqtt.util;
/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import com.lvmoney.frame.mqtt.common.constant.MqttCommonConstant;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import org.springframework.stereotype.Service;
/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
public class ChannelUtil {

    public static String getClientId(Channel channel) {
        return channel.attr(getClientId()).get();
    }

    public static void setClientId(Channel channel, String clientId) {
        channel.attr(getClientId()).set(clientId);
    }
    private static AttributeKey<String> getClientId() {
        return AttributeKey.valueOf(MqttCommonConstant.FIELD_MQTT_CLIENT_CLIENT_ID);
    }


    public static String getName(Channel channel) {
        return channel.attr(getName()).get();
    }

    public static void setName(Channel channel, String name) {
        channel.attr(getName()).set(name);
    }
    private static AttributeKey<String> getName() {
        return AttributeKey.valueOf(MqttCommonConstant.FIELD_MQTT_CLIENT_NAME);
    }

    public static String getPassword(Channel channel) {
        return channel.attr(getPassword()).get();
    }

    public static void setPassword(Channel channel, String password) {
        channel.attr(getPassword()).set(password);
    }
    private static AttributeKey<String> getPassword() {
        return AttributeKey.valueOf(MqttCommonConstant.FIELD_MQTT_CLIENT_PASSWORD);
    }

}
