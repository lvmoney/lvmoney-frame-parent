package com.lvmoney.frame.mqtt.common.constant;/**
 * 描述:
 * 包名:main.java.com.lvmoney.frame.mqtt.common.constant
 * 版本信息: 版本1.0
 * 日期:2024/10/14
 * Copyright 四川XXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/14 14:15
 */
public class MqttCommonConstant {
    /**
     * boss-thread name
     */
    public static final String NAME_BOSS_THREAD = "mqtt-boss-thread";
    /**
     * worker-thread name
     */
    public static final String NAME_WORKER_THREAD = "mqtt-worker-thread";
    /**
     * connRateLimiter name
     */
    public static final String NAME_CONN_LIMITER = "connRateLimiter";
    /**
     * trafficShaper name
     */
    public static final String NAME_TRAFFIC_SHAPER = "trafficShaper";

    /**
     * preludeHandler name
     */
    public static final String NAME_PRELUDE_HANDLER = "preludeHandler";


    /**
     * preludeHandler name
     */
    public static final String NAME_CONNECT_HANDLER = "MqttV3ConnectHandler";
    /**
     * preludeHandler name
     */
    public static final String NAME_MESSAGE_DEBOUNCE_HANDLER = "MqttMessageDebounceHandler";


    /**
     * ThreadLocalRandom.current().nextLong(3000, 5000)
     */
    public static final Long RANDOM_ORIGIN = 3000L;

    /**
     * ThreadLocalRandom.current().nextLong(3000, 5000)
     */
    public static final Long RANDOM_ROUND = 5000L;
    /**
     * 混淆码
     */
    public static final String PASS_CONFUSION_CODE = "LVMONEY";

    /**
     * mqtt user redis 前缀
     */
    public static final String MQTT_USER_GROUP_PREFIX = "mqttUser";

    /**
     * mqtt client redis 前缀
     */
    public static final String MQTT_CLIENT_GROUP_PREFIX = "mqttClient";


    /**
     * mqtt clientId 锁 前缀
     */
    public static final String MQTT_CLIENT_ID_LOCK_PREFIX = "mqttClientIdLock";

    /**
     * mqtt Subscription redis 前缀
     */
    public static final String MQTT_SUBSCRIPTION_PREFIX = "mqttSubscription";

    /**
     * mqtt client 字段
     */
    public static final String FIELD_MQTT_CLIENT_CLIENT_ID = "CLIENT_ID";


    /**
     * mqtt client 字段
     */
    public static final String FIELD_MQTT_CLIENT_NAME = "NAME";


    /**
     * mqtt client 字段
     */
    public static final String FIELD_MQTT_CLIENT_PASSWORD = "PASSWORD";

    /**
     * will topic
     */
    public static final String NAME_WILL_TOPIC = "willTopic";
    /**
     * qos 默认值
     */
    public static final Integer DEFAULT_QOS = 0;

    /**
     * retained 默认值
     */
    public static final Boolean DEFAULT_RETAINED = false;


    /**
     * mqtt client connect 前缀
     */
    public static final String MQTT_CONNECT_PREFIX = "mqttConnectState";



}
