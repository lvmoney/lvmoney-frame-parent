package com.lvmoney.frame.strategy.liteflow.context;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.context
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 16:26
 */
public class FieldContext {
    /**
     * 上下文传值
     */
    private final Map<String, Object> contextFields = new ConcurrentHashMap();

    public void setContextFields(String key, Object value) {
        contextFields.put(key, value);
    }

    public Object getContextFields(String key) {
        return contextFields.get(key);
    }

    public boolean hasStringData(String key) {
        return contextFields.containsKey(key);
    }

}
