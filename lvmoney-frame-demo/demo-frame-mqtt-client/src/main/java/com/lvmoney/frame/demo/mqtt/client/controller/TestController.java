package com.lvmoney.frame.demo.mqtt.client.controller;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.smqtt.client.controller
 * 版本信息: 版本1.0
 * 日期:2024/9/29
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.client.config.MqttConsumerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/29 15:33
 */
@RestController
public class TestController {
    @Autowired
    private MqttConsumerConfig client;

    @Value("${spring.mqtt.client.id}")
    private String clientId;

    @RequestMapping("/connect")
    @ResponseBody
    public String connect() {
        client.connect();
        return clientId + "连接到服务器";
    }

    @RequestMapping("/disConnect")
    @ResponseBody
    public String disConnect() {
        client.disConnect();
        return clientId + "与服务器断开连接";
    }

    @RequestMapping("/msg")
    @ResponseBody
    public String msg() {

        return clientId + "与服务器断开连接";
    }
}
