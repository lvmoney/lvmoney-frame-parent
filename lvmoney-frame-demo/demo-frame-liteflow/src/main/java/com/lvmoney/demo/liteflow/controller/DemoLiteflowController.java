package com.lvmoney.demo.liteflow.controller;/**
 * 描述:
 * 包名:com.lvmoney.demo.provider.function
 * 版本信息: 版本1.0
 * 日期:2020/3/5
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.demo.liteflow.service.FlowService;
import com.lvmoney.frame.base.core.api.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/3/5 15:23
 */
@RestController("com/lvmoney/demo/liteflow")
public class DemoLiteflowController {
    @Autowired
    FlowService flowService;

    @GetMapping(value = "test/{name}")
    public ApiResult<String> test(String name) {
        return ApiResult.success("test" + name);

    }

    @GetMapping(value = "exec")
    public void exec() {
        flowService.testConfig();
    }

}
