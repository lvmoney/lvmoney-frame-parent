package com.lvmoney.demo.liteflow.test;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.test
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.demo.liteflow.ao.TestAo;
import com.lvmoney.demo.liteflow.service.FlowService;
import com.lvmoney.demo.liteflow.vo.TestVo;
import com.lvmoney.frame.base.core.util.JsonUtil;
import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 15:09
 */
@Component("c")
public class CCmp extends NodeComponent {
    @Autowired
    FlowService flowService;

    @Override
    public void process() {
        TestAo testAo = new TestAo();
        testAo.setNo(10000);
        TestVo test = flowService.getTest(testAo);
        System.out.println(JsonUtil.objToJsonStr(test));

        System.out.println("Exec CCmp");
    }
}

