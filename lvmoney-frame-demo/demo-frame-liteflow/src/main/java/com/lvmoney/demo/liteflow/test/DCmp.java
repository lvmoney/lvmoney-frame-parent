package com.lvmoney.demo.liteflow.test;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.test
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.demo.liteflow.context.OrderContext;
import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 16:00
 */
@Component("d")
public class DCmp extends NodeComponent {

    @Override
    public void process() {
        OrderContext orderContext = this.getContextBean(OrderContext.class);
        Set<String> set = orderContext.getSet();
        System.out.println("ok");


    }
}
