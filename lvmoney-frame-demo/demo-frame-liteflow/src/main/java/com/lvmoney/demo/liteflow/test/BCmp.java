package com.lvmoney.demo.liteflow.test;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.test
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import com.yomahub.liteflow.core.NodeComponent;
import org.springframework.stereotype.Component;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0
 * 2024/9/27 15:09
 */
@Component("b")
public class BCmp extends NodeComponent {
    @Override
    public void process() {
        System.out.println("Exec BCmp");
    }
}

