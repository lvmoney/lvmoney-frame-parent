package com.lvmoney.demo.liteflow.vo;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.vo
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 15:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestVo implements Serializable {
    private String code;
}
