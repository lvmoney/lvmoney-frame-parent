package com.lvmoney.demo.liteflow.service;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.test
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.demo.liteflow.ao.TestAo;
import com.lvmoney.demo.liteflow.vo.TestVo;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 15:13
 */
public interface FlowService {

    void testConfig();


    TestVo getTest(TestAo testAo);
}
