package com.lvmoney.demo.liteflow.context;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.context
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import cn.hutool.core.collection.ConcurrentHashSet;

import java.util.Set;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 15:25
 */
public class OrderContext {
    private Set<String> set = new ConcurrentHashSet<>();

    public void add2Set(String str) {
        set.add(str);
    }

    public Set<String> getSet() {
        return set;
    }
}
