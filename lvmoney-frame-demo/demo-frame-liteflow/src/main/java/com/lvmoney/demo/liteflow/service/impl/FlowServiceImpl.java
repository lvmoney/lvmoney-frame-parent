package com.lvmoney.demo.liteflow.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.strategy.liteflow.test
 * 版本信息: 版本1.0
 * 日期:2024/9/27
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.demo.liteflow.ao.TestAo;
import com.lvmoney.demo.liteflow.context.OrderContext;
import com.lvmoney.demo.liteflow.service.FlowService;
import com.lvmoney.demo.liteflow.vo.TestVo;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/27 15:13
 */
@Component
public class FlowServiceImpl implements FlowService {
    @Autowired
    private FlowExecutor flowExecutor;

    @Override
    public void testConfig() {
        LiteflowResponse response = flowExecutor.execute2Resp("chain1", "", OrderContext.class);

    }

    @Override
    public TestVo getTest(TestAo testAo) {

        TestVo testVo = new TestVo();
        testVo.setCode("" + testAo.getNo() + 9000);
        return testVo;
    }
}
