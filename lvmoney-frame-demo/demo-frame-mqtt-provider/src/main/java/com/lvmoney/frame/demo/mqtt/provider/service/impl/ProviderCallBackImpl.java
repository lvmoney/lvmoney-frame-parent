package com.lvmoney.frame.demo.mqtt.provider.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.frame.demo.mqtt.provider.service.impl
 * 版本信息: 版本1.0
 * 日期:2024/10/18
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.provider.config.AbsProviderCallBack;
import org.springframework.stereotype.Service;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/18 14:17
 */
@Service
public class ProviderCallBackImpl extends AbsProviderCallBack {
}
