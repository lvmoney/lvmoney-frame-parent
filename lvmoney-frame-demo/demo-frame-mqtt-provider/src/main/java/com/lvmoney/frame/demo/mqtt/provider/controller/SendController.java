package com.lvmoney.frame.demo.mqtt.provider.controller;/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.smqtt.provider.controller
 * 版本信息: 版本1.0
 * 日期:2024/9/29
 * Copyright 四川XXXX科技有限公司
 */


import com.lvmoney.frame.mqtt.provider.config.MqttProviderConfig;
import com.lvmoney.frame.mqtt.provider.dto.PublishMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/9/29 15:21
 */
@Controller
public class SendController {
    @Autowired
    private MqttProviderConfig providerClient;

    @RequestMapping("/sendMessage")
    @ResponseBody
    public String sendMessage(int qos, boolean retained, String topic, String message) {
        try {
            PublishMessageDto publishMessageDto = new PublishMessageDto();
            publishMessageDto.setQos(qos).setRetained(retained).setTopic(topic).setMessage(message);
            providerClient.publish(publishMessageDto);
            return "发送成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "发送失败";
        }
    }
}
