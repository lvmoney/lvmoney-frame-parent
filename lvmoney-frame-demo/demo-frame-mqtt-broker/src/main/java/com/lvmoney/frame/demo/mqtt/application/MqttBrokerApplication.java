package com.lvmoney.frame.demo.mqtt.application;
/**
 * 描述:
 * 包名:com.lvmoney.frame.mqtt.service
 * 版本信息: 版本1.0
 * 日期:2024/10/15
 * Copyright 四川XXXX科技有限公司
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * @describe：
 * @author: lvmoney/四川XXXX科技有限公司
 * @version:v1.0 2024/10/11 16:17
 */
@SpringBootApplication
@ComponentScan(basePackages = {
        "com.lvmoney.**"
})
public class MqttBrokerApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(MqttBrokerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MqttBrokerApplication.class, args);
        LOGGER.info("==================== mqtt broker 启动完成 ====================");
    }

}